﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Model.Entity;
using Model.Entity.Material;
using Model.Enum;

namespace Model
{
    public class PumaContext : DbContext
    {
        public PumaContext(DbContextOptions<PumaContext> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Material> Materials { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<ObjectModelTracking> ObjectModelTracking { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ObjectModel>()
                .HasDiscriminator<string>(x => x.Type)
                .HasValue<Equipment>("equipment")
                .HasValue<Material>("material");

            modelBuilder.Entity<Material>().Property(x => x.MaterialCategory)
                .HasConversion(new EnumToStringConverter<CategoryMaterial>());
            modelBuilder.Entity<Material>().Property(x => x.State)
                .HasConversion(new EnumToStringConverter<StateObjectModel>());
            modelBuilder.Entity<Equipment>().Property(x => x.EquipmentCategory)
                .HasConversion(new EnumToStringConverter<CategoryEquipment>());
            modelBuilder.Entity<Equipment>().Property(x => x.State)
                .HasConversion(new EnumToStringConverter<StateObjectModel>());
            modelBuilder.Entity<Order>().Property(x => x.State)
                .HasConversion(new EnumToStringConverter<StateOrder>());

            modelBuilder.Entity<ObjectModelTracking>().HasKey(x => x.Id);
            modelBuilder.Entity<ObjectModelTracking>()
                .HasOne(x => x.ModelEquipment)
                .WithMany(x => x.Borrows)
                .HasForeignKey(x => x.ModelEquipmentId);
            modelBuilder.Entity<ObjectModelTracking>()
                .HasOne(x => x.Employee)
                .WithMany(x => x.Borrows)
                .HasForeignKey(x => x.EmployeeId);
        }
    }
}