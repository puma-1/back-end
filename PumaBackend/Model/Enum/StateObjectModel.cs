﻿namespace Model.Enum
{
    public enum StateObjectModel
    {
        NEW,
        DAMAGED,
        LOST,
        DEFICIENT
    }
}
