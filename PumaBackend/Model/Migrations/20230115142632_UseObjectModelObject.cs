﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Model.Migrations
{
    public partial class UseObjectModelObject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Equipments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Materials",
                table: "Materials");

            migrationBuilder.RenameTable(
                name: "Materials",
                newName: "ObjectModel");

            migrationBuilder.RenameColumn(
                name: "category",
                table: "ObjectModel",
                newName: "objectmodel_type");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpirationDate",
                table: "ObjectModel",
                type: "datetime(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AddColumn<string>(
                name: "EquipmentCategory",
                table: "ObjectModel",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "MaterialCategory",
                table: "ObjectModel",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "ObjectModel",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ObjectModel",
                table: "ObjectModel",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Stock_ModelEquipmentId",
                table: "Stock",
                column: "ModelEquipmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stock_ObjectModel_ModelEquipmentId",
                table: "Stock",
                column: "ModelEquipmentId",
                principalTable: "ObjectModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stock_ObjectModel_ModelEquipmentId",
                table: "Stock");

            migrationBuilder.DropIndex(
                name: "IX_Stock_ModelEquipmentId",
                table: "Stock");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ObjectModel",
                table: "ObjectModel");

            migrationBuilder.DropColumn(
                name: "EquipmentCategory",
                table: "ObjectModel");

            migrationBuilder.DropColumn(
                name: "MaterialCategory",
                table: "ObjectModel");

            migrationBuilder.DropColumn(
                name: "Reference",
                table: "ObjectModel");

            migrationBuilder.RenameTable(
                name: "ObjectModel",
                newName: "Materials");

            migrationBuilder.RenameColumn(
                name: "objectmodel_type",
                table: "Materials",
                newName: "category");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpirationDate",
                table: "Materials",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Materials",
                table: "Materials",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Equipments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Brand = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Description = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ExpirationDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    IsFav = table.Column<bool>(type: "bit(1)", nullable: false),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Photo = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PurchaseUrl = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PurchasedDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Reference = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    State = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SupplierName = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SupplierReference = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    category = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipments", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
