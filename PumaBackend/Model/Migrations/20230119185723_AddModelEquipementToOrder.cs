﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Model.Migrations
{
    public partial class NomDeMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ModelEquipementId",
                table: "Order",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_Order_ModelEquipementId",
                table: "Order",
                column: "ModelEquipementId");

            migrationBuilder.AddForeignKey(
                name: "FK_Order_ObjectModel_ModelEquipementId",
                table: "Order",
                column: "ModelEquipementId",
                principalTable: "ObjectModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order_ObjectModel_ModelEquipementId",
                table: "Order");

            migrationBuilder.DropIndex(
                name: "IX_Order_ModelEquipementId",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "ModelEquipementId",
                table: "Order");
        }
    }
}
