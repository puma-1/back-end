﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Model.Migrations
{
    public partial class UpdateModelEquipmentInOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order_ObjectModel_ModelEquipementId",
                table: "Order");

            migrationBuilder.RenameColumn(
                name: "ModelEquipementId",
                table: "Order",
                newName: "ModelEquipmentId");

            migrationBuilder.RenameIndex(
                name: "IX_Order_ModelEquipementId",
                table: "Order",
                newName: "IX_Order_ModelEquipmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Order_ObjectModel_ModelEquipmentId",
                table: "Order",
                column: "ModelEquipmentId",
                principalTable: "ObjectModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order_ObjectModel_ModelEquipmentId",
                table: "Order");

            migrationBuilder.RenameColumn(
                name: "ModelEquipmentId",
                table: "Order",
                newName: "ModelEquipementId");

            migrationBuilder.RenameIndex(
                name: "IX_Order_ModelEquipmentId",
                table: "Order",
                newName: "IX_Order_ModelEquipementId");

            migrationBuilder.AddForeignKey(
                name: "FK_Order_ObjectModel_ModelEquipementId",
                table: "Order",
                column: "ModelEquipementId",
                principalTable: "ObjectModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
