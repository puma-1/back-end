﻿using Model.Entity.GenericObject;
using Model.Entity.Material;

namespace Model.Entity
{
    public class Employee : IdentityObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Department { get; set; }
        public Guid? ManagerId { get; set; }
        public Guid BadgeiD { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime? DepartureDate { get; set; }
        public ICollection<ObjectModelTracking>? Borrows { get; set; }
    }
}
