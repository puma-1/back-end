﻿using Model.Entity.GenericObject;
using Model.Entity.Material;
using Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class Order : IdentityObject
    {
        public StateOrder State { get; set; }
        
        public ObjectModel ModelEquipment { get; set; }

        public int Quantity { get; set; }
    }
}
