﻿using Model.Entity.GenericObject;
using Model.Entity.Material;

namespace Model.Entity;

public class Stock : IdentityObject
{
    public ObjectModel ModelEquipment { get; set; }
    public int DesiredQuantity { get; set; }
    public int PhysicalQuantity { get; set; }
    public int VirtualQuantity { get; set; }
    public int MinimumQuantity { get; set; }
    public int ShelfNumber { get; set; }
    public int RowNumber { get; set; }
    public int ColumnNumber { get; set; }
} 