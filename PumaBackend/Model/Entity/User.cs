﻿using Model.Entity.GenericObject;

namespace Model.Entity
{
    public class User : IdentityObject
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
