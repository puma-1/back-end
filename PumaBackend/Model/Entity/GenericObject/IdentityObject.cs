﻿namespace Model.Entity.GenericObject
{
    public class IdentityObject
    {
        public Guid Id { get; set; }
    }
}