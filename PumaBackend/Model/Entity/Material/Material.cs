﻿using Model.Enum;

namespace Model.Entity.Material
{
    public class Material : ObjectModel
    {
        public CategoryMaterial MaterialCategory { get; set; }
    }
}
