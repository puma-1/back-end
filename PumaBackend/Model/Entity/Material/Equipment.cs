﻿using Model.Enum;

namespace Model.Entity.Material
{
    public class Equipment : ObjectModel
    {
        public string Reference { get; set; }
        public CategoryEquipment EquipmentCategory { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
