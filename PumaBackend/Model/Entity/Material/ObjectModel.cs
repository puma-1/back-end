﻿using Model.Entity.GenericObject;
using Model.Enum;

namespace Model.Entity.Material
{
    public class ObjectModel : IdentityObject
    {
        public string Name { get; set; }
        public string Type{ get; set; }
        public string SupplierName { get; set; }
        public string SupplierReference { get; set; }
        public string Brand { get; set; }
        public string Description { get; set; }
        public string PurchaseUrl { get; set; }
        public string Photo { get; set; }
        public DateTime PurchasedDate { get; set; }
        public bool IsFav { get; set; }
        public StateObjectModel State { get; set; }
        public ICollection<ObjectModelTracking>? Borrows { get; set; }
    }
}
