﻿using System.ComponentModel.DataAnnotations.Schema;
using Model.Entity.GenericObject;

namespace Model.Entity.Material;

public class ObjectModelTracking : IdentityObject
{
   public Guid EmployeeId { get; set; }
    public Guid ModelEquipmentId { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public virtual ObjectModel ModelEquipment { get; set; }
    public virtual Employee Employee { get; set; }
}