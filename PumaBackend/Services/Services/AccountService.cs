﻿using Model.Entity;
using Repository.Interfaces;
using System.Web.Helpers;
using Services.Interfaces;

namespace Services.Services;

public class AccountService : IAccountService
{
    protected readonly IGenericRepository<User> UserRepository;


    public AccountService(IGenericRepository<User> userRepository)
    {
        UserRepository = userRepository;
    }

    public bool Login(User user)
    {
        var existing = UserRepository.GetAll(x => x.Login == user.Login).FirstOrDefault();
        if (existing != null)
        {
            return Crypto.VerifyHashedPassword(existing?.Password, user.Password);
        }
        return false;
    }

    public User Add(User user)
    {
        user.Password = Crypto.HashPassword(user.Password);
        UserRepository.Add(user);
        return user;
    }
}