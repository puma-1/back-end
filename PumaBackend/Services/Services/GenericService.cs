﻿using Microsoft.AspNetCore.JsonPatch;
using Model.Entity.GenericObject;
using Repository.Interfaces;
using Services.Interfaces;
using System.Linq.Expressions;

namespace Services.Services
{
    public class GenericService<T> : IGenericService<T> where T : IdentityObject
    {
        protected readonly IGenericRepository<T> GenericRepository;
        public GenericService(IGenericRepository<T> genericRepository)
        {
            GenericRepository = genericRepository;
        }
        public virtual void Add(T obj)
        {
            GenericRepository.Add(obj);
        }

        public virtual bool Remove(Guid id)
        {
            return GenericRepository.Remove(id);
        }

        public T Get(Guid id)
        {
            return GenericRepository.Get(id);
        }

        public List<T> GetAll(Expression<Func<T, bool>> condition)
        {
            return GenericRepository.GetAll(condition);
        }

        public T Patch(Guid id, T updated)
        {
            var obj = Get(id);
            var patch = new JsonPatchDocument();
            if (obj != null)
            {
                if(obj.GetType() == typeof(T))
                {
                    foreach (var property in obj.GetType().GetProperties()
                        .Where(x => x.Name != "Id").ToList())
                    {
                        if(property.GetValue(updated) != null)
                        {
                            if (property.PropertyType == typeof(Guid))
                            {
                                Guid guid= (Guid)property.GetValue(updated);
                                if (guid == Guid.Empty) 
                                    continue;
                            }
                            if(property.PropertyType.BaseType == typeof(IdentityObject))
                            {
                                continue;
                            }
                            patch.Add(property.Name, property.GetValue(updated));
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

            GenericRepository.Patch(obj, patch);
            return obj;
        }

        public bool Exists(Guid id)
        {
            return GenericRepository.Exists(id);
        }
    }
}
