﻿using Model.Entity;
using Repository.Interfaces;
using Services.Interfaces;

namespace Services.Services;

public class EmployeeService : GenericService<Employee>, IEmployeeService
{
    public EmployeeService(IGenericRepository<Employee> genericRepository) : base(genericRepository)
    {
    }
    public string GetFullName(Guid id)
    {
        var person = GenericRepository.Get(id);
        return $"{person.FirstName} {person.LastName}";
    }


}