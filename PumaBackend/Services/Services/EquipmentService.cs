﻿using Model.Entity.Material;
using Repository.Interfaces;
using Services.Interfaces;

namespace Services.Services
{
    public class EquipmentService : GenericService<Equipment>,  IEquipmentService
    {
        public EquipmentService(IGenericRepository<Equipment> genericRepository) : base(genericRepository)
        {
        }

        public List<Equipment> GetExpired(DateTime? date = null)
        {
            if (date == null)
            {
                date = DateTime.Today;
            }
            return GenericRepository.GetAll(x => x.ExpirationDate < date);
        }
    }
}
