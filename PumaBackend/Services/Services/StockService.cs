﻿using Model.Entity;
using Repository.Interfaces;
using Services.Interfaces;

namespace Services.Services;

public class StockService : GenericService<Stock>, IStockService
{
    public StockService(IGenericRepository<Stock> genericRepository) : base(genericRepository)
    {
    }
    
    public Stock GetByModelEquipmentId(Guid modelEquipmentId)
    {
        return GenericRepository.GetAll(x => x.ModelEquipment.Id == modelEquipmentId).FirstOrDefault() ?? new Stock
        {
            PhysicalQuantity = 0
        };
    }

    public void DecreaseQuantity(Stock stock)
    {
        stock.PhysicalQuantity--;
        stock.VirtualQuantity--;
        Patch(stock.Id, stock);
    }

    public void IncreaseQuantity(Stock stock)
    {
        stock.PhysicalQuantity++;
        stock.VirtualQuantity++;
        Patch(stock.Id, stock);
    }
}