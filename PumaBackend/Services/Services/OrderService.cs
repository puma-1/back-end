﻿using Model.Entity;
using Model.Entity.Material;
using Repository.Interfaces;
using Services.Interfaces;

namespace Services.Services
{
    public class OrderService : GenericService<Order>, IOrderService
    {
        public OrderService(IGenericRepository<Order> genericRepository) : base(genericRepository)
        {
        }

        public List<Order> GetCurrentOrders()
        {
            return GenericRepository.GetAll(x => x.State == Model.Enum.StateOrder.INPROGRESS);
        }

        public List<Order> GetAutomaticOrders()
        {
            return GenericRepository.GetAll(x => x.State == Model.Enum.StateOrder.PENDING);
        }
    }
}
