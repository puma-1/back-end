﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Services.Interfaces
{
    public interface IGenericService<T>
    {
        void Add(T obj);
        bool Remove(Guid id);
        T Get(Guid id);
        List<T> GetAll(Expression<Func<T, bool>> condition = null);
        public T Patch(Guid id, T updated);
        bool Exists(Guid id);
    }
}