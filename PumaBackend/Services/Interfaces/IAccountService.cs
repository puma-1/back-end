﻿using Model.Entity;

namespace Services.Interfaces;

public interface IAccountService
{
    bool Login(User user);
    User Add(User user);
}