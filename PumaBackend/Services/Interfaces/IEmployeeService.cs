﻿using Model.Entity;

namespace Services.Interfaces;

public interface IEmployeeService 
{
    string GetFullName(Guid id);
}