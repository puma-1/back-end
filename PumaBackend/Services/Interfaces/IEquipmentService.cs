﻿using Model.Entity.Material;

namespace Services.Interfaces
{
    public interface IEquipmentService
    {
        public List<Equipment> GetExpired(DateTime? date = null);
    }
}
