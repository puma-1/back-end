﻿using Model.Entity;

namespace Services.Interfaces;

public interface IStockService
{
    public Stock GetByModelEquipmentId(Guid modelEquipmentId);
    public void DecreaseQuantity(Stock stock);
    public void IncreaseQuantity(Stock stock);
}