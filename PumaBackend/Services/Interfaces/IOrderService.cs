﻿using Model.Entity;

namespace Services.Interfaces
{
    public interface IOrderService
    {
        public List<Order> GetCurrentOrders();

        public List<Order> GetAutomaticOrders();

    }
}
