﻿using Microsoft.AspNetCore.JsonPatch;
using System.Linq.Expressions;

namespace Repository.Interfaces
{
    public interface IGenericRepository<T>
    {
        void Add(T obj);
        bool Remove(Guid id);
        T Get(Guid id);
        List<T> GetAll(Expression<Func<T, bool>> condition);
        void Patch(T obj, JsonPatchDocument patch);
        bool Exists(Guid id);
        bool Archive(Guid id);
    }
}