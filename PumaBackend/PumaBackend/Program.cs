using Microsoft.EntityFrameworkCore;
using Model;
using PumaBackend.DependencyInjector;

var builder = WebApplication.CreateBuilder(args);
var di = new DependencyInjector();

var config = builder.Configuration;
var services = builder.Services;

// Add services to the container.
di.InjectAll(services, config);
services.AddControllers().AddNewtonsoftJson(
    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);

var cs = config.GetConnectionString("Puma");
services.AddEntityFrameworkMySql().AddDbContext<PumaContext>(opt =>
    opt.UseMySql(cs, ServerVersion.AutoDetect(cs)));

services.AddEndpointsApiExplorer();
services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger(); 
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<PumaContext>();
    context.Database.Migrate();
}

app.Run();