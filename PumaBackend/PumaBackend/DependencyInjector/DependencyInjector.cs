﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Interfaces;
using Repository.Repository;
using Services.Interfaces;
using Services.Services;

namespace PumaBackend.DependencyInjector
{
    public class DependencyInjector
    {
        public void InjectAll(IServiceCollection services, IConfiguration configuration)
        {
            InjectServices(services);
            InjectRepositories(services);
        }

        public void InjectRepositories(IServiceCollection services)
        {
            services.AddScoped<DbContext, PumaContext>();
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
        }

        public void InjectServices(IServiceCollection services)
        {
            services.AddScoped(typeof(IGenericService<>), typeof(GenericService<>));
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IEquipmentService, EquipmentService>();
            services.AddScoped<IStockService, StockService>();
            services.AddScoped<IAccountService, AccountService>();
        }
    }
}
