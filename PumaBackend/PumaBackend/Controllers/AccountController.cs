﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("validateCredentials")]
        public IActionResult Login([FromBody]User user)
        {
            var exist =  _accountService.Login(user);
            return Ok(exist);
        }

        [HttpPost]
        public IActionResult Add([FromBody] User user)
        {
            var added = _accountService.Add(user);
            return Ok(added);
        }

    }
}
