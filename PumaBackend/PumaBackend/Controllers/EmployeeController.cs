﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    [Route("api/[controller]")]
    public class EmployeeController : GenericController<Employee>
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IGenericService<Employee> genericService, IEmployeeService employeeService) : base(genericService)
        {
            _employeeService = employeeService;
        }

        [HttpGet("fullname/{id}")]
        public IActionResult FullName(Guid id)
        {
            var fullname = _employeeService.GetFullName(id);
            return Json(fullname);
        }

        [HttpGet("term/{term}")]
        public IActionResult ByTerm(string term)
        {
            var res = _genericService.GetAll(x => x.FirstName.Contains(term)|| x.LastName.Contains(term) || x.Login.Contains(term));
            return Ok(res);
        }
    }
}
