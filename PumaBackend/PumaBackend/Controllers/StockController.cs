﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    [Route("api/[controller]")]
    public class StockController : GenericController<Stock>
    {
        private readonly IStockService _stockService;
        public StockController(IGenericService<Stock> genericService, IStockService stockService) : base(genericService)
        {
            _stockService = stockService;
        }

        [HttpGet("term/{term}")]
        public IActionResult ByTerm(string term)
        {
            var res = _genericService.GetAll(x => x.ModelEquipment.Name.Contains(term));
            return Ok(res);
        }
       
        [HttpGet("modelEquipment/{modelEquipmentId}")]
        public IActionResult GetByModelEquipmentId(Guid modelEquipmentId)
        {
            var res = _stockService.GetByModelEquipmentId(modelEquipmentId);
            return Ok(res);
        }
    }
}

