﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity.Material;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    [Route("api/[controller]")]
    public class EquipmentController : GenericController<Equipment>
    {
        private readonly IEquipmentService service;

        public EquipmentController(IGenericService<Equipment> genericService, IEquipmentService service) : base(genericService)
        {
            this.service = service;
        }

        [HttpGet("expired")]
        public IActionResult Expired(DateTime? date = null)
        {
            var s = service.GetExpired(date);
            return Ok(s);
        }
    }
}
