﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity;
using Model.Enum;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : GenericController<Order>
    {
        public OrderController(IGenericService<Order> genericService) : base(genericService)
        {
        }

        [HttpGet("currentOrders")]
        public IActionResult GetCurrentOrders()
        {
            var res = _genericService.GetAll(x => x.State == Model.Enum.StateOrder.INPROGRESS);
            return Ok(res);
        }

        [HttpGet("pendingOrders")]
        public IActionResult GetAutomaticOrders()
        {
            var res = _genericService.GetAll(x => x.State == Model.Enum.StateOrder.PENDING);
            return Ok(res);
        }

        [HttpGet("all/{term}")]
        public IActionResult AllByTerm(string term)
        {
            var res = _genericService.GetAll(x => x.ModelEquipment.Name.Contains(term));
            return Ok(res);
        }

        [HttpGet("pending/{term}")]
        public IActionResult PendingByTerm(string term)
        {
            var res = _genericService.GetAll(x => x.ModelEquipment.Name.Contains(term) && x.State == StateOrder.PENDING);
            return Ok(res);
        }

        [HttpGet("current/{term}")]
        public IActionResult CurrentByTerm(string term)
        {
            var res = _genericService.GetAll(x => x.ModelEquipment.Name.Contains(term) && x.State == StateOrder.INPROGRESS);
            return Ok(res);
        }
    }
}

