﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity.Material;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers;

public class ObjectModelTrackingController : GenericController<ObjectModelTracking>
{
    private readonly IStockService _stockService;
    public ObjectModelTrackingController(IGenericService<ObjectModelTracking> genericService,
        IStockService stockService) : base(genericService)
    {
        _stockService = stockService;
    }
    
    [HttpGet("borrows/employee/{employeeId}")]
    public IActionResult GetBorrowsByEmployeeId(Guid employeeId)
    {
        var res = _genericService.GetAll(x => x.EmployeeId == employeeId);
        return Ok(res);
    }
    
    [HttpGet("borrows/modelEquipment/{modelEquipmentId}")]
    public IActionResult GetBorrowsByModelEquipmentId(Guid modelEquipmentId)
    {
        var res = _genericService.GetAll(x => x.ModelEquipmentId == modelEquipmentId);
        return Ok(res);
    }
   
    [HttpPost("borrows/assign/{employeeId}/{modelEquipmentId}")]
    public IActionResult AssignObjectModelToEmployee(Guid modelEquipmentId, Guid employeeId)
    {
        var stock = _stockService.GetByModelEquipmentId(modelEquipmentId);
        if (stock.PhysicalQuantity <= 1)
        {
            return BadRequest("No more items in stock");
        }
        
        var borrow = new ObjectModelTracking
        {
            ModelEquipmentId = modelEquipmentId,
            EmployeeId = employeeId,
            StartDate = DateTime.Now
        };
        
        _genericService.Add(borrow);
        _stockService.DecreaseQuantity(stock);
        return Ok();
    }

    [HttpPost("borrows/remove/{trackingId}/")]
    public IActionResult RemoveObjectModelFromEmployee(Guid trackingId)
    {
        var borrow = _genericService.GetAll(x => x.Id == trackingId).FirstOrDefault();
        if (borrow == null)
            return BadRequest();
        var stock = _stockService.GetByModelEquipmentId(borrow.ModelEquipmentId);

        borrow.EndDate = DateTime.Now;
        _genericService.Patch(borrow.Id, borrow);
        _stockService.IncreaseQuantity(stock);
        return Ok();
    }
}