﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity.Material;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    public class ObjectModelController : GenericController<ObjectModel>
    {
        public ObjectModelController(IGenericService<ObjectModel> genericService) : base(genericService)
        {
        }

        [HttpGet("term/{term}")]
        public IActionResult ByTerm(string term)
        {
            var res = _genericService.GetAll(x => x.Name.Contains(term));
            return Ok(res);
        }
    }
}
