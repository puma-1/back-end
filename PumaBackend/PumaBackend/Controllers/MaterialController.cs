﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity.Material;
using PumaBackend.Controllers.Generic;
using Services.Interfaces;

namespace PumaBackend.Controllers
{
    [Route("api/[controller]")]
    public class MaterialController : GenericController<Material>
    {
        public MaterialController(IGenericService<Material> genericService) : base(genericService)
        {
        }
    }
}
