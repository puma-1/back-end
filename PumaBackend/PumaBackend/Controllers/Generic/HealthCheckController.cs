﻿using Microsoft.AspNetCore.Mvc;

namespace PumaBackend.Controllers.Generic
{
    [Route("healthcheck")]
    [ApiController]
    public class HealthCheckController : Controller
    {
        [HttpGet]
        public IActionResult HealthCheck()
        {
            return Ok("Api fonctionne");
        }
    }
}
