﻿using Microsoft.AspNetCore.Mvc;
using Model.Entity.GenericObject;
using Services.Interfaces;

namespace PumaBackend.Controllers.Generic
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<T> : Controller where T : IdentityObject
    {
        protected readonly IGenericService<T> _genericService;

        public GenericController(IGenericService<T> genericService)
        {
            _genericService = genericService;
        }

        [HttpGet]
        public virtual IActionResult GetAll()
        {
            var objs = _genericService.GetAll();
            return Ok(objs);
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            if(id == Guid.Empty)
            {
                return BadRequest();
            }
            var obj = _genericService.Get(id);
            if (obj == null)
            {
                return NotFound();
            }

            return Ok(obj);
        }

        [HttpPost]
        public virtual IActionResult Post([FromBody] T obj)
        {
            _genericService.Add(obj);
            return Created($"stocked in table Employee with id {obj.Id}", obj);
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] T updated)
        {
            var obj = _genericService.Patch(id, updated);
            if(obj == null)
            {
                return NotFound("An error has been encountered during the update of the field in the API");
            }
            return Accepted(obj);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var res = _genericService.Remove(id);
            if (res)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}
